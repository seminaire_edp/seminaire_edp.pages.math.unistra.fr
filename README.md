# Page web du "Séminaire Equations aux dérivées partielles"

Disponible ici : https://seminaire_edp.pages.math.unistra.fr/

## Procédure pour construire la page d'accueil

### Présentation des fichiers

* `no_seminar.txt` contient la liste des mardis où il n'y aura pas de séminaire, sous le format `AAAA-MM-DD: <raison de l'absence de séminaire>`
* `generate_html.py` est un script `Python 3` qui permet de générer automatiquement le fichier `public/index.html`
* `template_index.html` est un *template* `html`, qui dicte la génération de `public/index.html` par `generate_html.py`

### Construction de `public/index.html`

* Les dépendances `jinja2` sont dans `requirements.txt`:

```bash
pip3 install -r requirements.txt
```

 * On peut ensuite lancer `generate_html.py` pour générer `public/index.html` :

```bash
python3 generate_html.py
```

 * `generate_html.py` est configuré pour traiter toutes les dates entre `2021-09-07` et (aujourd'hui + 60 jours): cf. variable `END_DATE` .

* Pour construire la page à partir d'un fichier données json local, il suffit de placer le fichier sur le chemin indiqué par `LOCAL_JSON_FILEPATH`. 