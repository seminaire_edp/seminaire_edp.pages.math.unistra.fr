import datetime

import pytest
from generate_html import get_slots_from_txt, parse_txt_file, update_slots


@pytest.fixture
def no_seminar(tmp_path):
    NO_SEMINAR_STRING = """\
2021-09-28: centenaire de l'IMU
2021-10-05: demi-journée de l'équipe MoCo
2021-10-26: vacances universitaires

"""
    no_seminar_path = tmp_path / "no_seminar.txt"
    with open(no_seminar_path, "w") as f:
        f.write(NO_SEMINAR_STRING)

    return no_seminar_path


def test_parse_txt_file(no_seminar):
    dates = parse_txt_file(no_seminar)
    assert dates == {
        datetime.date(2021, 9, 28): "centenaire de l'IMU",
        datetime.date(2021, 10, 5): "demi-journée de l'équipe MoCo",
        datetime.date(2021, 10, 26): "vacances universitaires",
    }


def test_get_slots_from_txt(no_seminar):
    assert {
        datetime.date(2021, 9, 7): {
            "abstract": "",
            "date": datetime.date(2021, 9, 7),
            "filled": False,
            "slides": "",
            "speaker": "",
            "summer": False,
            "title": "",
            "to_fill": True,
        },
        datetime.date(2021, 9, 14): {
            "abstract": "",
            "date": datetime.date(2021, 9, 14),
            "filled": False,
            "slides": "",
            "speaker": "",
            "summer": False,
            "title": "",
            "to_fill": True,
        },
        datetime.date(2021, 9, 21): {
            "abstract": "",
            "date": datetime.date(2021, 9, 21),
            "filled": False,
            "slides": "",
            "speaker": "",
            "summer": False,
            "title": "",
            "to_fill": True,
        },
    }.items() <= get_slots_from_txt(no_seminar).items()


def test_update_slots(no_seminar):
    slots = get_slots_from_txt(no_seminar)
    seminars = update_slots(slots)
    # from pprint import pprint
    # pprint(seminars[:3])
    print(seminars[-3:])
    assert seminars[-3:] == [
        {
            "abstract": "Proposée récemment comme une alternative à la DFT PPLB dans "
            "l'optique d'étudier des systèmes à nombre fractionnaire "
            "d'électrons, nous détaillerons la construction mathématique de "
            "la DFT N-centrée afin de pouvoir y étendre méthodes Kohn-Sham "
            "et Kohn-Sham généralisée utilisées en DFT N-électronique et "
            "aborder des problématiques annexes (limite semi-classique, "
            "dimère de Hubbard).",
            "date": datetime.date(2021, 9, 21),
            "start": "14h",
            "end": "15h",
            "filled": True,
            "slides": "slides/2021-09-21.pdf",
            "slides_exist": True,
            "speaker": "Guillaume Grente",
            "title": "Discussion autour des méthodes d'analyse convexe pour la théorie "
            "de la fonctionnelle de la densité (DFT) N-centrée",
            "to_fill": False,
        },
        {
            "abstract": "Nous nous intéresserons dans cet exposé à l'équation de "
            "Schrödinger logarithmique (abrégé en logNLS), équation "
            "non-linéaire introduite en 1976 par Bia&#322;ynicki-Birula et "
            "Mycielski dans un modèle de mécanique des ondes linéaires en "
            "physique. Longtemps oublié par les mathématiciens, cette "
            "équation présente une dynamique originale, parfois surprenante "
            "comparée à celle des équations de Schrödinger non-linéaires "
            "usuellement étudiées, dont les non-linéarités sont régulières "
            "voire lisses (typiquement du type puissance). J'exposerai "
            "quelques propriétés de logNLS qui attestent de cette "
            "originalité, en me focalisant sur les résultats de comportement "
            "en temps long. En particulier, sera présenté plus en profondeur "
            "le cas du régime dispersif, dont la compréhension du "
            "comportement en temps grand est très avancée : la vitesse de "
            "dispersion est plus rapide d'un facteur logarithmique et le "
            "carré du module de la solution renormalisée converge faiblement "
            "dans L^1 vers une gaussienne universelle, ne dépendant pas des "
            "conditions initiales. Je montrerai que cette description peut "
            "être améliorée par une vitesse de convergence explicite et "
            "optimale en distance de Wasserstein-1 (aussi appelé métrique de "
            "Kantorovich-Rubinstein), indépendante de la constante "
            "semi-classique, et que cette convergence est également valable "
            "à la limite semi-classique.",
            "date": datetime.date(2021, 9, 14),
            "start": "14h",
            "end": "15h",
            "filled": True,
            "slides": "slides/2021-09-14.pdf",
            "slides_exist": True,
            "speaker": "Guillaume Ferrière",
            "title": "Équation de Schrödinger logarithmique : dynamique en temps long, "
            "régime dispersif",
            "to_fill": False,
        },
        {
            "abstract": " À l’échelle microscopique, la locomotion à travers un fluide "
            "suit des lois différentes de celles de notre échelle (régime de "
            "Stokes). La recherche sur la natation de micro-organismes et la "
            "conception de micro-robots nageurs est notamment motivée par "
            "des applications biomédicales : par exemple, chirurgie "
            "non-invasive ou livraison d’une molécule à un endroit précis du "
            "corps. Cela nécessite bien sûr d’être capable de propulser et "
            "contrôler efficacement et précisément le micro-robot. La "
            "théorie du contrôle offre un bon cadre théorique pour répondre "
            "à ces problématiques. Ainsi, dans cet exposé, je présenterai "
            "quelques résultats de contrôlabilité et de contrôle de robots "
            "micro-nageurs. Dans un premier temps, j’étudierai la propulsion "
            "par champ magnétique externe pour un robot élastique, et "
            "montrerai qu’il n’est en général pas contrôlable au voisinage "
            "de sa position d’équilibre. Ensuite, je m’intéresserai au "
            "contrôle d’une particule à l’aide de la variation du flux "
            "environnant générée par un autre nageur ; je discuterai de la "
            "contrôlabilité du système associé et présenterai une méthode de "
            "planification de trajectoires.",
            "date": datetime.date(2021, 9, 7),
            "start": "14h",
            "end": "15h",
            "filled": True,
            "slides": "slides/2021-09-07.pdf",
            "slides_exist": True,
            "speaker": "Clément Moreau",
            "title": "Quelques problèmes de contrôle de micro-nageurs",
            "to_fill": False,
        },
    ]
