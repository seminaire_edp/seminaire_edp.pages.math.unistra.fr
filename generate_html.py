import json
import locale
import logging
import os
from datetime import datetime, timedelta
from pathlib import Path
from urllib import request

import jinja2

logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)

locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")

LOCAL_JSON_FILEPATH = Path("agenda_mysql.json")
JSON_URL = "https://irma.pages.math.unistra.fr/agenda/agenda_mysql.json"

START_DATE = datetime(2021, 9, 7).date()
END_DATE = datetime(2025, 12, 20).date()  # until end of December
# END_DATE = (datetime.now() + timedelta(60)).date()  # Today + 60 days

SUMMERS_START = [  # start of summer pause (included)
    datetime(2022, 6, 14).date(),
    datetime(2023, 6, 8).date(),
    datetime(2024, 6, 3).date(),
    datetime(2025, 6, 9).date(),
]
SUMMERS_END = [  # end of summer pause (excluded)
    datetime(2022, 9, 6).date(),
    datetime(2023, 9, 18).date(),
    datetime(2024, 9, 17).date(),
    datetime(2025, 9, 3).date(),
]
SUMMER_REASON = "pause estivale"

FORCE_ADD_DATES = []  # force add some dates


class DateTimeDecoder(json.JSONDecoder):
    """From https://gist.github.com/abhinav-upadhyay/5300137"""

    def __init__(self, *args, **kargs):
        json.JSONDecoder.__init__(self, object_hook=self.dict_to_object, *args, **kargs)

    def dict_to_object(self, d):
        if "__type__" not in d:
            return d

        type = d.pop("__type__")
        try:
            date_object = datetime(**d)
            return date_object
        except TypeError:
            d["__type__"] = type
            return d


def parse_txt_file(no_seminar_filename: str) -> dict:
    """Read no_seminar_file and return a dict {date: reason}"""

    no_seminars = {}
    with open(no_seminar_filename, "r") as no_seminar_file:
        for line in no_seminar_file:
            try:
                date_str, reason = line.split(":")
                date = datetime.strptime(date_str.strip(), "%Y-%m-%d").date()
                reason = reason.strip()
                no_seminars[date] = reason
            except ValueError:
                # empty line
                pass
    return no_seminars


def get_slots_from_txt(
    no_seminar_filename: str,
    start_date=START_DATE,
    end_date=END_DATE,
    summers_start=SUMMERS_START,
    summers_end=SUMMERS_END,
    no_seminar_string="Pas de séminaire : ",
) -> dict:
    """Return a dict of empty or no seminar slots"""

    no_seminars = parse_txt_file(no_seminar_filename)

    seminars = {}
    date = start_date

    while date < end_date:
        seminar = {
            "title": "",
            "abstract": "",
            "slides": "",
            "filled": False,
            "date": date,
            "speaker": "",
            "summer": False,
        }

        if date in FORCE_ADD_DATES:
            seminar["to_fill"] = True
            seminar["speaker"] = ""

        else:
            for summer_start, summer_end in zip(summers_start, summers_end):
                if summer_start <= date < summer_end:
                    seminar["to_fill"] = False
                    seminar["summer"] = True
                    seminar["speaker"] = no_seminar_string + SUMMER_REASON

            else:
                try:
                    reason = no_seminars[date]
                    seminar["to_fill"] = False
                    seminar["speaker"] = no_seminar_string + reason
                except KeyError:
                    seminar["to_fill"] = True
                    seminar["speaker"] = ""

        seminars[date] = seminar
        date += timedelta(days=7)

    return seminars


def load_json() -> dict:
    """
    Read local or remote version of agenda as json file
    and return data as dict
    """
    try:
        with open(LOCAL_JSON_FILEPATH, "r") as json_file:
            json_string = json_file.read()
            logging.info(f"Reading local {LOCAL_JSON_FILEPATH} file")
    except FileNotFoundError:
        with request.urlopen(JSON_URL) as url:
            json_string = url.read()
            logging.info(f"Reading remote {JSON_URL} file")

    return json.loads(json_string, cls=DateTimeDecoder)


def find_seminar_in_json(seminars: list) -> list:
    """Return EDP seminar event list from all seminars"""
    for seminar in seminars:
        if seminar["title"] == "Equations aux dérivées partielles":
            return seminar["events"]
    raise ValueError("No PDE seminar found in seminars")


def get_seminar(PDE_seminar: dict) -> dict:
    """Return a seminar dict from json data"""

    seminar_date = PDE_seminar["date"].date()
    seminar_start_hour = PDE_seminar["date"].hour

    if (seminar_start_min := PDE_seminar["date"].minute) != 0:
        seminar_start_time = f"{seminar_start_hour}h{seminar_start_min}"
        seminar_end_time = f"{seminar_start_hour + 1}h{seminar_start_min}"
    else:
        seminar_start_time = f"{seminar_start_hour}h"
        seminar_end_time = f"{seminar_start_hour + 1}h"

    seminar = {
        "filled": True,
        "to_fill": False,
        "date": seminar_date,
        "start": seminar_start_time,
        "end": seminar_end_time,
    }

    for field in "speaker", "title", "abstract":
        seminar[field] = PDE_seminar[field]

    # seminar['slides'] = f"slides/{seminar_date}.pdf"
    seminar["slides"] = os.path.join("slides", f"{seminar_date}.pdf")
    seminar["slides_exist"] = os.path.isfile(os.path.join("public", seminar["slides"]))
    return seminar


def update_slots(slots: dict, start_date=START_DATE) -> list:
    """
    Return a list of seminars by updating slots with data
    from json file
    """

    # Load updated data from json file
    agenda_json = load_json()
    all_seminars_json = agenda_json["seminars"]
    seminars_json = find_seminar_in_json(all_seminars_json)

    # Build the {date: seminar} dict
    json_seminars_by_date = {}
    for json_seminar in seminars_json:
        date = json_seminar["date"].date()
        if date >= start_date:
            json_seminars_by_date[date] = get_seminar(json_seminar)

    # Get a sorted list of all dates
    all_dates = sorted(set(list(json_seminars_by_date.keys()) + list(slots.keys())))

    # Build updated list
    seminars = []
    for date in all_dates:
        try:
            # Try to find seminar in json data
            seminar = json_seminars_by_date[date]
        except KeyError:
            # Fall back to default slots
            seminar = slots[date]
        seminars.append(seminar)

    seminars.reverse()

    return seminars


def create_html(seminars: list, html_filename="public/index.html"):
    """Generate html file from template and seminars data"""

    env = jinja2.Environment(loader=jinja2.FileSystemLoader("."))

    template = env.get_template("template_index.html")
    html_out = template.render(talks=seminars)

    out_path = Path(html_filename)
    out_path.write_text(html_out)
    logging.info(f"Written file: {out_path}")


def main():
    slots = get_slots_from_txt("no_seminar.txt")
    seminars = update_slots(slots)
    create_html(seminars)
    logging.info("done!")


if __name__ == "__main__":
    main()
